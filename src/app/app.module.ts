import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {AccountListComponent} from './account/account-list/account-list.component';
import {AccountRowComponent} from './account/account-row/account-row.component';
import {AccountEditorComponent} from './account/account-editor/account-editor.component';
import {AccountDetailsComponent} from './account/account-details/account-details.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {FormsModule} from '@angular/forms';
import {TransactionListComponent} from './transaction/transaction-list/transaction-list.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TransactionRowComponent} from './transaction/transaction-row/transaction-row.component';
import {TransactionEditorComponent} from './transaction/transaction-editor/transaction-editor.component';

@NgModule({
  declarations: [
    AppComponent,
    AccountListComponent,
    AccountRowComponent,
    AccountEditorComponent,
    AccountDetailsComponent,
    DashboardComponent,
    TransactionListComponent,
    TransactionRowComponent,
    TransactionEditorComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
