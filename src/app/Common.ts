import {environment} from '../environments/environment';

export const ACCOUNT_URL = environment.base_url + '/account';
export const TRANSACTION_URL = environment.base_url + '/transaction';
