import {Component, OnInit} from '@angular/core';
import {Transaction} from '../Transaction';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {TransactionService} from '../transaction.service';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-transaction-editor',
  templateUrl: './transaction-editor.component.html',
  styleUrls: ['./transaction-editor.component.css']
})
export class TransactionEditorComponent implements OnInit {

  transaction: Transaction;
  transactionSaved = new Subject();

  constructor(public transactionService: TransactionService,
              public activeModal: NgbActiveModal) {
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.transactionService
      .saveTransaction(this.transaction)
      .subscribe(value => {
        this.activeModal.close(value);
        this.transactionSaved.next();
      });
  }

  onCancel(): void {
    this.activeModal.dismiss();
  }

  dateChanged(eventDate: string): Date | null {
    return !!eventDate ? new Date(eventDate) : null;
  }
}
