import {Component, Input, OnInit} from '@angular/core';
import {Transaction} from '../Transaction';
import {TransactionType} from '../TransactionType';

@Component({
  selector: 'app-transaction-row',
  templateUrl: './transaction-row.component.html',
  styleUrls: ['./transaction-row.component.css']
})
export class TransactionRowComponent implements OnInit {

  @Input()
  transaction: Transaction;

  typeEnum = TransactionType;

  constructor() { }

  ngOnInit(): void {
  }

}
