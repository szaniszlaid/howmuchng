import {Injectable} from '@angular/core';
import {Transaction} from './Transaction';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {TRANSACTION_URL} from '../Common';
import {ErrorHandlerService} from '../account/error-handler.service';
import {TransactionType} from './TransactionType';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(private errorHandler: ErrorHandlerService,
              private http: HttpClient) {
  }

  createNewTransaction(accountId: number): Transaction {
    const transaction = new Transaction(accountId);
    transaction.type = TransactionType.EXPENSE;
    transaction.createdAt = new Date();
    return transaction;
  }

  findByAccount(accountId: number): Observable<Array<Transaction>> {
    return this.http
      .get<Array<Transaction>>(TRANSACTION_URL, {params: {accountId: accountId.toString()}})
      .pipe(
        catchError(errorRes => {
          this.errorHandler.errorSubject.next(errorRes.error.message);
          return throwError(errorRes);
        }))
      .pipe(
        map(value =>
          value.sort((a, b) => a.createdAt < b.createdAt ? 1 : -1))
      );
  }

  saveTransaction(transaction: Transaction): Observable<Transaction> {
    return this.http
      .post<Transaction>(TRANSACTION_URL, transaction)
      .pipe(catchError(errorRes => {
        this.errorHandler.errorSubject.next(errorRes.error.message);
        return throwError(errorRes);
      }));
  }
}
