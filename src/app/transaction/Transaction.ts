import {TransactionType} from './TransactionType';

export class Transaction {
  id?: number;
  accountId: number;
  createdAt: Date;
  price: number;
  description: string;
  type: TransactionType;


  constructor(accountId: number) {
    this.accountId = accountId;
  }
}
