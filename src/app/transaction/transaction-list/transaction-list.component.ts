import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Transaction} from '../Transaction';
import {TransactionService} from '../transaction.service';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TransactionEditorComponent} from '../transaction-editor/transaction-editor.component';

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.css']
})
export class TransactionListComponent implements OnInit {

  @Output() transactionChange = new EventEmitter();

  transactions: Observable<Array<Transaction>>;
  accountId: number;

  constructor(private transactionService: TransactionService,
              private activatedRoute: ActivatedRoute,
              private modalService: NgbModal) {
  }

  ngOnInit(): void {
    if (this.activatedRoute.snapshot.paramMap.has('id')) {
      this.accountId = +this.activatedRoute.snapshot.paramMap.get('id');
      this.loadTransactions();
    }
  }

  loadTransactions(): void {
    this.transactions = this.transactionService.findByAccount(this.accountId);
  }

  newTransaction(): void {
   this.openTransactionEditor(this.transactionService.createNewTransaction(this.accountId));
  }

  editTransaction(transaction: Transaction): void {
    this.openTransactionEditor({...transaction});
  }

  openTransactionEditor(transaction: Transaction): void {
    const modalRef = this.modalService.open(TransactionEditorComponent, {centered: true, backdrop: 'static'});
    modalRef.componentInstance.transaction = transaction;
    modalRef.result
      .then(() => this.loadTransactions())
      .catch(reason => console.log(reason))
      .finally(() => modalRef.close());

    modalRef.componentInstance.transactionSaved.subscribe(() => this.transactionChange.emit());
  }
}
