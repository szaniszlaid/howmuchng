import {Component, Input, OnInit} from '@angular/core';
import {Account} from '../Account';


@Component({
  selector: 'app-account-row',
  templateUrl: './account-row.component.html',
  styleUrls: ['./account-row.component.css']
})
export class AccountRowComponent implements OnInit {

  @Input()
  account: Account;

  constructor() { }

  ngOnInit(): void {
  }

}

