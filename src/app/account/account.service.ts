import {Injectable} from '@angular/core';
import {Account} from './Account';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {ACCOUNT_URL} from '../Common';
import {ErrorHandlerService} from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private errorHandler: ErrorHandlerService,
              private http: HttpClient) {
  }

  addAccount(account: Account): Observable<Account> {
    console.log('Adding new account');

    return this.postAccount(account)
      .pipe(map(id => {
        account.id = id;
        console.log('New account has been added successfully.');
        return account;
      }));
  }

  getAccountById(paramId: number): Observable<Account> {
    return this.fetchAccountById(paramId);
  }

  updateAccount(account: Account): Observable<void> {
    return this.putAccount(account);
  }

  getAccounts(): Observable<Array<Account>> {
    return this.fetchAccounts();
  }

  removeAccount(id: number): Observable<void> {
    return this.deleteAccount(id);
  }

  getAccountBalance(accountId: number): Observable<number> {
    return this.fetchAccountBalance(accountId);
  }

  // HTTP

  private fetchAccountById(id: number): Observable<Account> {
    return this.http
      .get<Account>(ACCOUNT_URL + '/' + id)
      .pipe(catchError(errorRes => {
        this.errorHandler.errorSubject.next(errorRes.error.message);
        return throwError(errorRes);
      }));
  }

  private fetchAccounts(): Observable<Array<Account>> {
    return this.http
      .get<Array<Account>>(ACCOUNT_URL)
      .pipe(catchError(errorRes => {
        this.errorHandler.errorSubject.next(errorRes.error.message);
        return throwError(errorRes);
      }));
  }


  private postAccount(account: Account): Observable<number> {
    return this.http
      .post<number>(ACCOUNT_URL, account)
      .pipe(catchError(errorRes => {
        console.log(errorRes.error.message);
        this.errorHandler.errorSubject.next(errorRes.error.message);
        return throwError(errorRes);
      }));
  }

  private putAccount(account: Account): Observable<void> {
    return this.http
      .put<void>(ACCOUNT_URL, account)
      .pipe(catchError(errorRes => {
        this.errorHandler.errorSubject.next(errorRes.error.message);
        return throwError(errorRes);
      }));
  }

  private deleteAccount(id: number): Observable<void> {
    return this.http
      .delete<void>(ACCOUNT_URL + '/' + id)
      .pipe(catchError(errorRes => {
        this.errorHandler.errorSubject.next(errorRes.error.message);
        return throwError(errorRes);
      }));
  }

  private fetchAccountBalance(accountId: number): Observable<number> {
    return this.http
      .get<number>(ACCOUNT_URL + '/' + accountId + '/balance')
      .pipe(catchError(errorRes => {
        this.errorHandler.errorSubject.next(errorRes.error.message);
        return throwError(errorRes);
      }));
  }
}
