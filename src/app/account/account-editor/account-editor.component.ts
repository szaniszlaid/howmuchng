import {Component, OnInit} from '@angular/core';
import {Account} from '../Account';
import {AccountService} from '../account.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-account-editor',
  templateUrl: './account-editor.component.html',
  styleUrls: ['./account-editor.component.css']
})
export class AccountEditorComponent implements OnInit {

  // comes from param
  account: Account;
  editing: boolean;

  constructor(private router: Router,
              private accountService: AccountService,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    if (this.activatedRoute.snapshot.paramMap.has('id')) {
      const paramId = +this.activatedRoute.snapshot.paramMap.get('id');
      this.accountService.getAccountById(paramId).subscribe(value => this.account = value);
      this.editing = true;
    } else {
      this.editing = false;
      this.account = new Account();
    }
  }

  onSubmit(value: Account): void {
    if (this.editing) {
      value.id = this.account.id;
      this.accountService
        .updateAccount(value)
        .subscribe(() => this.navigateToAccounts());
    } else {
      this.accountService
        .addAccount(value)
        .subscribe(savedAccount => {
          this.account = savedAccount;
          this.navigateToAccounts();
        });
    }
  }

  onCancel(): void {
    this.navigateToAccounts();
  }

  private navigateToAccounts(): void {
    let url = '/accounts/';
    if (this.account.id) {
      url += this.account.id;
    }

    this.router
      .navigate([url], {replaceUrl: true})
      .catch(reason => console.log(reason));
  }
}
