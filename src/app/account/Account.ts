export class Account {
  id?: number;
  name?: string;
  balance = 0;
  openingBalance = 0;
  description?: string;
}
