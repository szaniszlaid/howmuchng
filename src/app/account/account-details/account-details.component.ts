import {Component, OnInit} from '@angular/core';
import {Account} from '../Account';
import {ActivatedRoute, Router} from '@angular/router';
import {AccountService} from '../account.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.css']
})
export class AccountDetailsComponent implements OnInit {

  account: Observable<Account>;
  accountId: number;
  balance: number;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private accountService: AccountService) {
  }

  ngOnInit(): void {
    if (this.activatedRoute.snapshot.paramMap.has('id')) {
      this.accountId = +this.activatedRoute.snapshot.paramMap.get('id');
      this.account = this.accountService.getAccountById(this.accountId);
      this.initBalance();
    }
  }

  initBalance(): void {
    this.accountService.getAccountBalance(this.accountId).subscribe(value => this.balance = value);
  }

  editAccount(id: number): void {
    this.router.navigate(['accounts/edit/' + id]);
  }

  deleteAccount(id: number): void {
    this.accountService.removeAccount(id)
      .subscribe(() => this.router.navigate(['accounts'], {replaceUrl: true}));
  }
}
