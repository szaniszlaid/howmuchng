import {Component, OnDestroy, OnInit} from '@angular/core';
import {Account} from '../Account';
import {AccountService} from '../account.service';
import {NavigationEnd, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {filter} from 'rxjs/operators';


@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css']
})
export class AccountListComponent implements OnInit {

  accounts: Account[] = [];

  constructor(private accountService: AccountService, private router: Router) { }

  ngOnInit(): void {
    this.accountService.getAccounts().subscribe(accounts => this.accounts = accounts);

  }

  onNewAccount(): void {
    this.router.navigate(['accounts/edit']);
  }

}

