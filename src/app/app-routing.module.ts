import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AccountListComponent} from './account/account-list/account-list.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AccountEditorComponent} from './account/account-editor/account-editor.component';
import {AccountDetailsComponent} from './account/account-details/account-details.component';

const routes: Routes = [
  {path: '', component: DashboardComponent},
  {path: 'accounts', component: AccountListComponent},
  {path: 'accounts/edit',  component: AccountEditorComponent},
  {path: 'accounts/edit/:id',  component: AccountEditorComponent},
  {path: 'accounts/:id',  component: AccountDetailsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
