import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {AccountService} from './account/account.service';
import {NavigationEnd, Router} from '@angular/router';
import {filter} from 'rxjs/operators';
import {ErrorHandlerService} from './account/error-handler.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'HowMuchNgClient';

  private navigationEndSub: Subscription;
  private errorSub: Subscription;

  error: string;


  constructor(private errorHandler: ErrorHandlerService,
              private accountService: AccountService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.errorSub = this.errorHandler.errorSubject.subscribe(error => {
      return this.error = error;
    });

    this.navigationEndSub = this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(() => this.clearError());
  }

  clearError(): void {
    this.error = null;
  }

  ngOnDestroy(): void {
    this.errorSub.unsubscribe();
    this.navigationEndSub.unsubscribe();
  }

}
